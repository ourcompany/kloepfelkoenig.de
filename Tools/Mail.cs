﻿using System.Collections.Generic;
using System.Net.Mail;
using ourCompany;

namespace Website.Tools
{
    public class Mail
    {
        public readonly Dictionary<string, string> CSS = new Dictionary<string, string>();

        public Mail()
        {
            CSS.Add("body", "background-color:#f4f4f4;color:#444444;font-size:13px;font-family:serif;");
            CSS.Add(".main-table", "width:600px; background-position:right 10px; background-repeat:no-repeat; background-color:white; margin:25px 50px 150px 25px; padding:15px;box-shadow: -1px 1px 3px rgba(0,0,0,0.3);");
        }

        public string HTMLBody { get; set; }
        public string PlainBody { get; set; }
        public string Recipient { get; set; }
        public string Subject { get; set; }

        /// <summary>
        /// Sends the Mail
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static void Send(Mail email)
        {
            var smtp = new SmtpClient();
            var message = _Render(email);
            smtp.SendCompleted += (s, error) => message.Dispose();
            smtp.Send(message);
        }

        /// <summary>
        /// Sends the Mail Asynchroniously
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public static void SendAsync(Mail email)
        {
            var smtp = new SmtpClient();
            var message = _Render(email);

            smtp.SendCompleted += (s, error) => message.Dispose();
            smtp.SendAsync(message, null);
        }

        private static MailMessage _Render(Mail mail)
        {
            var html =
                new CSSControl()
                    .Parse("html")
                        .Parse("head")
                            .AddAttribute("type", "text/css")
                            .Parse("style")
                                .Apply(c =>
                                {
                                    foreach (var cssClass in mail.CSS)
                                    {
                                        c.WriteLine("{0} {{ {1} }}".With(cssClass.Key, cssClass.Value));
                                    }
                                })
                            .Parse("/style")
                        .Parse("/head")
                        .Parse("body")
                            .Parse("table.main-table")
                                .Parse("tr")
                                    .Parse("td")
                                        .Parse("table")
                                            .Write(mail.HTMLBody)
                                        .Parse("/table")
                                    .Parse("/td")
                                .Parse("/tr")
                            .Parse("/table")
                        .Parse("/body")
                    .Parse("/html")
                    .ToString();

            var message = new MailMessage
            {
                Subject = mail.Subject,
                Body = mail.PlainBody
            };
            message.From = new MailAddress(message.From.Address, message.From.DisplayName);
            message.To.Add(new MailAddress(mail.Recipient));
            message.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, new System.Net.Mime.ContentType("text/html")));
            return message;
        }
    }
}