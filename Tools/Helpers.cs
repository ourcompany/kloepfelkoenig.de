﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using MongoDB.Bson;
using MongoDB.Driver;
using ourCompany;

namespace Website.Tools
{
    public static class Helpers
    {
        private const string _AXIS_ATTRIBUTE = "_axis";
        private const string _CHILDREN_ATTRIBUTE = "_children";
        private const string _ID_ATTRIBUTE = "_id";
        private const string _PARENT_ATTRIBUTE = "_parent";
        private const string _ROOT_COLLECTION_NAME = "root";

        #region comparers

        public class DateTimeComparer : IComparer<BsonDocument>
        {
            private string _Key;

            public DateTimeComparer(string key)
            {
                _Key = key;
            }

            public int Compare(BsonDocument x, BsonDocument y)
            {
                return DateTime.Compare(y.GetValue(_Key).ToUniversalTime(), x.GetValue(_Key).ToUniversalTime());
            }
        }

        #endregion comparers

        #region IDictionary

        /// <summary>
        /// Returns the value for the key or null if not exists
        /// </summary>
        /// <typeparam name="TKey"></typeparam>
        /// <typeparam name="TElement"></typeparam>
        /// <param name="dictionary"></param>
        /// <param name="key"></param>
        /// <returns>the value for the key or null if not exists</returns>
        public static TElement TryGetValue<TKey, TElement>(this IDictionary<TKey, TElement> dictionary, TKey key) where TElement : class
        {
            return dictionary.ContainsKey(key) ? dictionary[key] : null;
        }

        #endregion IDictionary

        #region Localizations

        /// <summary>
        /// Gets the best language depending on the request language preferences.
        /// </summary>
        /// <param name="req">The request</param>
        /// <param name="availableLangs">The available languages.</param>
        /// <returns></returns>
        public static string GetLanguageString(IEnumerable<string> prefered, IEnumerable<string> available)
        {
            if (prefered == null || prefered.Count() == 0)
                return available.First();

            return prefered
                       .Select(l => available.FirstOrDefault(lang => l.ToLowerInvariant().Contains(lang.ToLowerInvariant())))
                       .FirstOrDefault(l => l.Check()) ?? available.First();
        }

        ///// <summary>
        ///// Gets the link of the current page in another language
        ///// </summary>
        ///// <param name="env">The IEnvironement.</param>
        ///// <param name="newLang">The new language.</param>
        ///// <returns></returns>
        //public static string ChangeLang(IEnvironement env, string newLang)
        //{
        //    var prefix = (Configuration.Languages.Count() > 1 ? "/" + env.Language.IsoCode : "");
        //    return "/" + prefix + (env.Rest ?? "");
        //}

        #endregion Localizations

        #region Strings

        static readonly private Regex _StripTagsRegex = new Regex(@"<[^>]*>", RegexOptions.Compiled);
        static readonly private Regex _StripWhiteSpacesRegex = new Regex(" {2,}", RegexOptions.Compiled);

        /// <summary>
        /// Ellipsises the specified text.
        /// </summary>
        /// <param name="text">The text.</param>
        /// <param name="length">The max length of the text.</param>
        /// <returns></returns>
        static public string Ellipsis(this string text, int length)
        {
            if (text.Length <= length) return text;
            var pos = text.IndexOf(" ", length, StringComparison.Ordinal);
            if (pos >= 0)
                return text.Substring(0, pos) + "…";
            return text;
        }

        /// <summary>
        /// Strips all tags in the string.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <returns></returns>
        public static string StripTags(this string source)
        {
            return !source.Check() ? "" : _StripWhiteSpacesRegex.Replace(_StripTagsRegex.Replace(source, " "), " ").Trim();
            //var array = new char[source.Length];
            //var arrayIndex = 0;
            //var inside = false;
            //foreach (var @let in source)
            //{
            //    if (@let == '<') { inside = true; continue; }
            //    if (@let == '>') { inside = false; continue; }
            //    if (inside) continue;
            //    array[arrayIndex] = @let;
            //    arrayIndex++;
            //}
            //return new string(array, 0, arrayIndex);
        }

        /// <summary>
        /// Replaces the format items in a specified string with the string representations of
        /// corresponding objects in a specified array. A first parameter supplies culture-specific
        /// formatting information.
        /// </summary>
        /// <param name="input">A composite format string.</param>
        /// <param name="culture">The culture.</param>
        /// <param name="parameters">An object array that contains zero or more objects to format.</param>
        /// <returns></returns>
        public static string With(this string input, System.Globalization.CultureInfo culture, params object[] parameters)
        {
            return string.Format(culture, input, parameters);
        }

        #endregion Strings

        #region JSON

        private static JavaScriptSerializer _LazySerializer;

        private static JavaScriptSerializer _Serializer
        {
            get
            {
                return _LazySerializer ?? (_LazySerializer = new JavaScriptSerializer());
            }
        }

        /// <summary>
        /// Translate the object in a JSON form
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns></returns>
        public static string ToJSON(this object obj)
        {
            return _Serializer.Serialize(obj);
        }

        #endregion JSON

        #region Slimmage

        static public CSSControl LazyResponsiveImage(this CSSControl control, string src, string className = "", string alt = "")
        {
            if (src.Split('?')[0].EndsWith(".gif"))
                return
                    control
                        .AddAttribute("src", src.Split('?')[0])
                        .AddAttribute("class", className)
                        .AddAttribute("alt", alt)
                        .Tag("img");
            return control
                    .AddAttribute("data-img-class", className)
                    .AddAttribute("data-img-src", src)
                    .Parse("noscript")
                        .AddAttribute("class", className)
                        .AddAttribute("src", src)
                        .AddAttribute("alt", alt)
                        .Tag("img")
                    .Parse("/noscript");
        }

        static public CSSControl ResponsiveImage(this CSSControl control, string src, string className = "", string alt = "")
        {
            if (src.Split('?')[0].EndsWith(".gif"))
                return
                    control
                        .AddAttribute("src", src.Split('?')[0])
                        .AddAttribute("class", className)
                        .AddAttribute("alt", alt)
                        .Tag("img");
            return control
                    .AddAttribute("data-slimmage", "data-slimmage")
                    .LazyResponsiveImage(src, className, alt);
        }

        #endregion Slimmage

        #region srcset

        public static CSSControl Img(this CSSControl control, string src, string sizes, string alt = "", string resizerQuery = "", int minwidth = 150, int maxwidth = 2650, int steps = 5)
        {
            var delta = (int)Math.Floor((double)(maxwidth - minwidth) / steps);
            var srcset = new StringBuilder();

            for (var i = 0; i < steps; i++)
            {
                var width = (delta * i) + minwidth;
                srcset.AppendFormat("{0}?width={1}&{2} {1}w".With(src, width, resizerQuery));
                if (i < steps)
                    srcset.Append(",");
            }

            control
                .AddAttribute("alt", alt)
                .AddAttribute("srcset", srcset.ToString())
                .AddAttribute("sizes", sizes)
                .AddAttribute("src", src + "?maxwidth=" + maxwidth)
                .Tag("img");

            return control;
        }

        #endregion
        /// <summary>
        /// shortcut to not(string.IsNullOrEmpty)
        /// </summary>
        /// <param name="s">string</param>
        /// <returns></returns>
        public static bool Check(this string s)
        {
            return !string.IsNullOrEmpty(s);
        }

        // <summary>
        /// Return a string using the format using the specified arguments </summary> <param
        /// name="format">The format.</param> <param name="args">The args.</param> <returns></returns>
        public static string With(this string format, params object[] args)
        {
            return string.Format(format, args);
        }
    }
}