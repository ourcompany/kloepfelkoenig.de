﻿using System;
using System.Collections.Generic;
using System.Globalization;
using ImageResizer;
using ImageResizer.Plugins;

namespace Website.Tools
{
    public class Ratio : IPlugin, IQuerystringPlugin, ISettingsModifier
    {
        public IEnumerable<string> GetSupportedQuerystringKeys()
        {
            return new[] { "ratio" };
        }

        public IPlugin Install(ImageResizer.Configuration.Config c)
        {
            c.Plugins.add_plugin(this);
            return this;
        }

        public ResizeSettings Modify(ResizeSettings settings)
        {
            string
                width = settings["width"],
                height = settings["height"],
                queryRatio = settings["ratio"];
            double
                ratio,
                value;

            if (!double.TryParse(queryRatio, NumberStyles.Any, CultureInfo.InvariantCulture, out ratio))
                return settings;

            if (double.TryParse(width, out value))
            {
                settings["height"] = Math.Ceiling(value / ratio).ToString(CultureInfo.InvariantCulture);
                settings["mode"] = "crop";
                settings["crop"] = "auto";
            }
            else if (double.TryParse(height, out value))
            {
                settings["width"] = Math.Ceiling(value * ratio).ToString(CultureInfo.InvariantCulture);
                settings["mode"] = "crop";
                settings["crop"] = "auto";
            }
            return settings;
        }

        public bool Uninstall(ImageResizer.Configuration.Config c)
        {
            c.Plugins.remove_plugin(this);
            return true;
        }
    }
}