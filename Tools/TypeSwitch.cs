﻿using System;

namespace Website.Tools
{
    public static class TypeSwitch
    {
        public static Switch<TSource> On<TSource>(TSource value)
        {
            return new Switch<TSource>(value);
        }

        public static SwitchOnType OnType<TSource>()
        {
            return new SwitchOnType(typeof(TSource));
        }

        public static SwitchOnType OnType(System.Type type)
        {
            return new SwitchOnType(type);
        }

        public class SwitchOnType
        {
            private bool handled = false;
            private System.Type type;

            internal SwitchOnType(System.Type type)
            {
                this.type = type;
            }

            public SwitchOnType Case<TTarget>(Action action)
            {
                if (!handled)
                {
                    var targetType = typeof(TTarget);
                    if (targetType.IsAssignableFrom(type))
                    {
                        action();
                        handled = true;
                    }
                }

                return this;
            }

            public void Default(Action action)
            {
                if (!handled)
                    action();
            }
        }
    }

    public class Switch<TSource>
    {
        private bool handled = false;
        private TSource value;

        internal Switch(TSource value)
        {
            this.value = value;
        }

        public Switch<TSource> Case<TTarget>(Action<TTarget> action)
            where TTarget : TSource
        {
            if (!handled)
            {
                var sourceType = value.GetType();
                var targetType = typeof(TTarget);
                if (targetType.IsAssignableFrom(sourceType))
                {
                    action((TTarget)value);
                    handled = true;
                }
            }

            return this;
        }

        public void Default(Action<TSource> action)
        {
            if (!handled)
                action(value);
        }
    }
}