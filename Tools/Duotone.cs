﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using ImageResizer.Configuration;
using ImageResizer.Plugins;
using ImageResizer.Resizing;

namespace Website.Tools
{
    public class Duotone : BuilderExtension, IPlugin, IQuerystringPlugin
    {
        public static Color ParseColor(string value, Color defaultValue)
        {
            var c = ParseColor(value);
            return c ?? defaultValue;
        }

        public static Color? ParseColor(string value)
        {
            if (string.IsNullOrEmpty(value)) return null;
            value = value.TrimStart('#');
            //try hex first
            int val;
            if (int.TryParse(value, NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture, out val))
            {
                var alpha = 255;
                if (value.Length != 4 && value.Length != 8)
                    return Color.FromArgb(alpha, ColorTranslator.FromHtml("#" + value));

                var regLength = value.Length - (value.Length / 4);
                alpha = int.Parse(value.Substring(regLength), NumberStyles.AllowHexSpecifier, CultureInfo.InvariantCulture);
                if (regLength == 3) alpha *= 16;
                value = value.Substring(0, regLength);
                return Color.FromArgb(alpha, ColorTranslator.FromHtml("#" + value));
            }
            try
            {
                var c = ColorTranslator.FromHtml(value); //Throws an 'Exception' instance if invalid
                return (c.IsEmpty) ? null : (Color?)c;
            }
            catch
            {
                return null;
            }
        }

        public IEnumerable<string> GetSupportedQuerystringKeys()
        {
            return new[] { "duotone", "duotone.contrast", "duotone.brightness" };
        }

        public IPlugin Install(Config c)
        {
            c.Plugins.add_plugin(this);
            return this;
        }

        public bool Uninstall(Config c)
        {
            c.Plugins.remove_plugin(this);
            return true;
        }

        /// <summary>
        /// Supporting rounded corners.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>

        protected override RequestedAction PostCreateImageAttributes(ImageState s)
        {
            if (s.copyAttibutes == null) return RequestedAction.None;

            if (!s.settings.WasOneSpecified((string[])GetSupportedQuerystringKeys())) return RequestedAction.None;

            var filters = new List<float[][]>();

            // make grayscale

            var color = ParseColor(s.settings["duotone"]) ?? Color.Black;

            var contrast = s.settings["duotone.contrast"];
            var brightness = s.settings["duotone.brightness"];
            double temp;

            filters.Add(_DuoToneMatrix(color));
            if (!string.IsNullOrEmpty(contrast) && double.TryParse(contrast, NumberStyles.Any, NumberFormatInfo.InvariantInfo, out temp)) filters.Add(_Contrast((float)temp));
            if (!string.IsNullOrEmpty(brightness) && double.TryParse(brightness, NumberStyles.Any, NumberFormatInfo.InvariantInfo, out temp)) filters.Add(_Brightness((float)temp));

            filters.Add(_GrayscaleFlat());

            switch (filters.Count)
            {
                case 0:
                    return RequestedAction.None;

                case 1:
                    s.copyAttibutes.SetColorMatrix(new ColorMatrix(filters[0]));
                    break;

                default:
                    //Multiple all the filters
                    var first = filters[0];

                    for (var i = 1; i < filters.Count; i++)
                    {
                        first = _Multiply(first, filters[i]);
                    }
                    s.copyAttibutes.SetColorMatrix(new ColorMatrix(first));
                    break;
            }

            return RequestedAction.None;
        }

        private static float[][] _Brightness(float factor)
        {
            return new[]
        {
                                  new float[]{1,0,0,0,0},
                                  new float[]{0,1,0,0,0},
                                  new float[]{0,0,1,0,0},
                                  new float[]{0,0,0,1,0},
                                  new[]{factor,factor,factor,0,1}};
        }

        private static float[][] _Contrast(float c)
        {
            c++; //Stop at -1

            var factorT = 0.5f * (1.0f - c);

            return (new[]
        {
                                  new[]{c,0,0,0,0},
                                  new[]{0,c,0,0,0},
                                  new[]{0,0,c,0,0},
                                  new[]{0,0,0,1.0f,0},
                                  new[]{factorT,factorT,factorT,0,1}});
        }

        private static float[][] _DuoToneMatrix(Color color)
        {
            //var rFactor = 0.229f;
            //var gFactor = 0.587f;
            //var bFactor = 0.229f;

            var percentR = (255.0f - color.R) / 255.0f;
            var percentG = (255.0f - color.G) / 255.0f;
            var percentB = (255.0f - color.B) / 255.0f;

            return (new[] {
                    new[]{percentR,0,0,0,0},
                    new[]{0,percentG,0,0,0},
                    new[]{0,0,percentB,0,0},
                    new[]{0,0,0,1.0f,0},
                    new[]{color.R / 255.0f,color.G / 255.0f,color.B / 255.0f,0,1}});
        }

        private static float[][] _Grayscale(float r, float g, float b)
        {
            return (new[]
        {
                                  new[]{r,r,r,0,0},
                                  new[]{g,g,g,0,0},
                                  new[]{b,b,b,0,0},
                                  new float[]{0,0,0,1,0},
                                  new float[]{0,0,0,0,1}});
        }

        //Warming Filter (85) #EC8A00
        //Warming Filter (LBA) #FA9600
        //Warming Filter (81) #EBB113
        //Coolling Filter (80) #006DFF
        //Cooling Filter (LBB) #005DFF
        //Cooling Filter (82) #00B5FF
        //Red #EA1A1A
        //Orange #F38417
        //Yellow #F9E31C
        //Green #19C919
        //Cyan #1DCBEA
        //Blue #1D35EA
        //Violet #9B1DEA
        //Magenta #E318E3
        //Sepia #AC7A33
        //Deep Red #FF0000
        //Deep Blue #0022CD
        //Deep Emerald #008C00
        //Deep Yellow #FFD500
        //Underwater #00C1B1
        private static float[][] _GrayscaleFlat()
        {
            return _Grayscale(1f / 3f, 1f / 3f, 1f / 3f);
        }

        private static float[][] _Multiply(float[][] f1, float[][] f2)
        {
            var x = new float[5][];
            for (var d = 0; d < 5; d++)
                x[d] = new float[5];
            const int size = 5;
            var column = new float[5];
            for (var j = 0; j < 5; j++)
            {
                for (var k = 0; k < 5; k++)
                {
                    column[k] = f1[k][j];
                }
                for (var i = 0; i < 5; i++)
                {
                    var row = f2[i];
                    float s = 0;
                    for (var k = 0; k < size; k++)
                    {
                        s += row[k] * column[k];
                    }
                    x[i][j] = s;
                }
            }
            return x;
        }

        /*Y = 0.299 * R + 0.587 * G + 0.114 * B
    I = 0.596 * R - 0.274 * G - 0.322 * B
    Q = 0.212 * R - 0.523 * G + 0.311 * B
         */
    }
}