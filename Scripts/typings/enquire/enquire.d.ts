﻿/*
enquire.js :http://wicky.nillia.ms/enquire.js/
A lightweight, pure JavaScript library for responding to CSS media queries.
v 2.0

*/

interface EnquireInstance {
    register(query: string, handler: Function, shouldDegrade?: boolean): EnquireInstance;
    register(query: string, handler: EnquireHandler, shouldDegrade?: boolean): EnquireInstance;
    register(query: string, handler: Array<EnquireHandler>, shouldDegrade?: boolean): EnquireInstance;
    register(query: string, handler: Array<Function>, shouldDegrade?: boolean): EnquireInstance;

    unregister(query: string, handler: EnquireHandler): EnquireInstance;
    unregister(query: string, handler: Function): EnquireInstance;
}

interface EnquireHandler {
    match: Function;
    unmatch?: Function;
    destroy?: Function;
    setup?: Function;
    deferSetup?: boolean;
}

declare var enquire: EnquireInstance;