﻿/**
 * Enum for Directions
 * @enum {number} - flag values
 */
enum Direction {
    Center = 0,
    Top = 1,
    Bottom = 2,
    Right = 4,
    Left = 8,
    Over = 16,
    Under = 32
}

/**
 * Enum for keys keyboard charcode
 * @enum {number}
 */
enum KeyCharcode {
    Enter = 13,
    Escape = 27,

    Space = 32,

    Left = 37,
    Up = 38,
    Right = 39,
    Down = 40
}