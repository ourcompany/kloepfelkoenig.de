﻿import ResponsiveImages = require("Helpers/ResponsiveImages");
// ---------------------------------------------------------------------------------------------------
// DOM Ready
// ---------------------------------------------------------------------------------------------------
// kloepfelkoenig.de.DomReady();
// add pop stats HistoryManager to window
// ---------------------------------------------------------------------------------------------------
// Init
// ---------------------------------------------------------------------------------------------------

//enquire
//    .register("screen and (max-width: 600px)", {
//        match: () => {
//            kloepfelkoenig.de.Platform = kloepfelkoenig.de.Platforms.Mobile;
//            Events.Publish("ResponsiveChange");
//        }
//    })
//    .register("screen and (min-width: 601px) and (max-width: 1024px)", {
//        match: () => {
//            kloepfelkoenig.de.Platform = kloepfelkoenig.de.Platforms.Tablet;
//            Events.Publish("ResponsiveChange");
//        }
//    })
//    .register("screen and (min-width: 1025px) and (max-width: 1180px)", {
//        match: () => {
//            kloepfelkoenig.de.Platform = kloepfelkoenig.de.Platforms.Laptop;
//            Events.Publish("ResponsiveChange");
//        }
//    })
//    .register("screen and (min-width: 1181px)", {
//        match: () => {
//            kloepfelkoenig.de.Platform = kloepfelkoenig.de.Platforms.Desktop;
//            Events.Publish("ResponsiveChange");
//        }
//    }, true);
// ---------------------------------------------------------------------------------------------------
// Before Load
// ---------------------------------------------------------------------------------------------------
// kloepfelkoenig.de.BeforeLoad();

ResponsiveImages.Check();