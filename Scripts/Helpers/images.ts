var
    _SvgExtension = /.*\.svg$/,
    _SubstituteSVG = () => {
        {
            var imgs = <NodeListOf<HTMLImageElement>>document.getElementsByTagName('img');
            var l = imgs.length;
            while (l--) {
                var img = imgs[l];
                if (img.src.match(_SvgExtension)) {
                    var
                        noscript = document.createElement("noscript");

                    noscript.setAttribute("data-slimmage", "data-slimmage");
                    noscript.setAttribute("data-img-class", img.className);
                    noscript.setAttribute("data-img-src", img.src + '.png?max-width=1000');

                    img.parentElement.replaceChild(noscript, img);
                }
            }
        }
    };

export var Check = () => {
    if (!Modernizr.svg)
        _SubstituteSVG();
};
Check();
window.addEventListener("load", Check);