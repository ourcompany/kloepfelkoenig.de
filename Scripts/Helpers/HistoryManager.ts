﻿import Helpers = require("Helpers/Helpers");
export var Href: string = Helpers.NormalizeHref(((<any>window.history).location || document.location).href);

export var ChangeState = (url: string, title: string, state?: any) => {
    var href = Helpers.NormalizeHref(url);

    if (Href == href)
        return; // we are there already;

    Href = href;

    document.title = Helpers.DecodeHTML(title);

    if (ga)
        ga(function (tracker) {
            tracker.send('pageview',
                {
                    'page': href,
                    'title': title
                });
        });

    history.pushState(state, null, href);
}

export var OnPopState = (e: PopStateEvent) => {
    var
        url = ((<any>window.history).location || document.location).href,
        href = Helpers.NormalizeHref(url);

    if (Href != href) {
        Href = href;
        NavigateTo(href);
    }
}

export var NavigateTo = (url: string): void => {
    var href = Helpers.NormalizeHref(url);
    switch (href) {
        default:
            // go to href
            break;
    }
}