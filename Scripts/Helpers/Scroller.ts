﻿import Helpers = require("Helpers/Helpers");
import Animator = require("Helpers/Animator");
import EaseOutQuart = require("Helpers/Easing/EaseOutQuart")
var
    _Animator: Animator.Engine,
    _ScrollContainer = Helpers.GetScrollTopElement();

var _onFrame = (val: number) => {
    _ScrollContainer.scrollTop = val;
}

var _onEnd = (top: number) => {
    _onFrame(top);
    _Animator = null;
}

export var To = (val: number) => {
    if (_Animator != null)
        _Animator.Cancel();

    _Animator = new Animator.Engine(_ScrollContainer.scrollTop, val, 30, _onFrame, (a: Animator.Engine) => _onEnd(val), EaseOutQuart);
    _Animator.Start();
}

export var ToHtmlElt = (scrollElt: HTMLElement, minusHeader = false, offset = 0) => {
    var headerHeight = minusHeader ? Helpers.GetHeight(document.getElementById("header")) : 0;

    if (!scrollElt)
        return;

    var top = Helpers.GetTop(scrollElt) - headerHeight - offset;
    To(top);
}