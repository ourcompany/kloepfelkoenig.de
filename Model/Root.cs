﻿using ourCompany.cms.Model;

namespace Website.Model
{
    public class Root :
        RootNode,
        IParent<Example>,
        IDetachedParent<DetachedNode, DetachedNode.DetachedDef>
    {
    }
}