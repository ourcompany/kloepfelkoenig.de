﻿using ourCompany.cms.Model;
using ourCompany.cms.Widgets;

namespace Website.Model
{
    public class DetachedNode : Node
    {
        [Text]
        public string Title { get; set; }

        public class DetachedDef : DetachedDefinition<DetachedNode>
        {
            public DetachedDef()
            {
                Sorts.Add("Title", "{ Title : 1 }");
            }
        }
    }
}