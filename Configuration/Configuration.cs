﻿using System.Reflection;

namespace Website
{
    public static partial class Configuration
    {
        public static string CurrentVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly()
                                           .GetName()
                                           .Version
                                           .ToString();
            }
        }

        public static class InnerHTML
        {
            public const string StyleFormats = "[{\"title\": \"red\", \"inline\" : \"span\", \"classes\" : \"red\"}]";
            public const string CustomCSSPath = "/Styles/custom-styles.css";

        }



    }
}