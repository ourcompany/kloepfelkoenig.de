﻿using Website.App.Routing.Routes;
using Website.Layouts;
using Website.Layouts.Base;

namespace Website
{
    public static partial class Configuration
    {
        public static IRoute[] Routes = new IRoute[] {
           new PageRoute<Default> ("~/Layouts/Default.aspx")
                .Named("Startpage")
                .Matching("")
                .Stylesheet("common.css")
                .Renders<Views.Startpage>(),

            new PageRoute<Default> ("~/Layouts/Default.aspx")
                .Named("Content-Menu")
                .Matching("{Node:Example:UID}")
                .Stylesheet("common.css")
                .Renders<Views.ContentView>(),

            new PageRoute<Default> ("~/Layouts/Default.aspx")
                .Named("Content-Submenu")
                .Matching("{Node:Example:UID}/{SubNode:Child:UID}")
                .Stylesheet("common.css")
                .Renders<Views.ContentView>(),

             new PageRoute<Default> ("~/Layouts/Default.aspx")
                .Named("Constant and Variable")
                .Matching("/constant/{Node:Example:UID}/{TextVariable}")
                .Stylesheet("common.css")
                .Renders<Views.ContentView>()
        };
    }
}