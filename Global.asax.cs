﻿using System;
using System.Collections.Generic;
using System.Web;
using MongoDB.Driver;
using ourCompany.cms;
using System.Linq;

namespace Website
{
    public class Global : HttpApplication
    {
        // TODO: X-Browser

        public static Dictionary<string, string> Version = new Dictionary<string, string>();

        private static int _CmsVersion;


        public override string GetVaryByCustomString(HttpContext context, string args)
        {
            var final = new List<string> { };
            foreach (var arg in args.Split(';'))
            {
                if (arg == "cms")
                {
                    final.Add(_CmsVersion.ToString());
                    continue;
                }
                else if (arg.StartsWith("cms:"))
                {
                    var keys = arg.Substring(4).Split(',');
                    var result = "";
                    foreach (var key in keys)
                    {
                        if (!Version.ContainsKey(key))
                            Version.Add(key, DateTime.Now.ToString());
                        result += Version[key];
                    }
                    final.Add(result);
                    continue;
                }
                else if (arg == "port")
                {
                    final.Add(context.Request.Url.Port.ToString());
                }
            }
            return final.Any() ? string.Join("|", final) : base.GetVaryByCustomString(context, args);
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }

        protected void Application_End(object sender, EventArgs e)
        {
        }

        protected void Application_Error(object sender, EventArgs e)
        {
        }

        protected void Application_Start(object sender, EventArgs e)
        {
            ourCompany.cms.Events.Instance.Changed += _OnChange;
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Session_Start(object sender, EventArgs e)
        {
        }

        private static void _OnChange(Events sender, params MongoDBRef[] nodes)
        {
            foreach (var node in nodes)
            {
                var key = node.CollectionName + "/" + node.Id.ToString();
                if (Version.ContainsKey(key))
                    Version[key] = DateTime.Now.ToString();
                else
                    Version.Add(key, DateTime.Now.ToString());
            }
            _CmsVersion++;
        }
    }
}