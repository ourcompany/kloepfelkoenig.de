﻿using System.Linq;
using Microsoft.Owin;
using ourCompany.cms;
using ourCompany.cms.Data;
using Owin;
using Website.App.Localization;
using Website.App.Routing;

[assembly: OwinStartup(typeof(kloepfelkoenig.de.App_Start.Startup))]

namespace Website.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.CreatePerOwinContext(MongoContext.CreateFromConfig);

            var config = app.UseOurCMS();

            Router.Languages = config.Databases.Select(db => new Language(db.Language).ConnectionString(db.DBName, db.MongoClientSettings.ToString())).ToArray();
            Router.IsPreview = (context) => context.HttpContext.Request.QueryString["preview"] == "true";
            Router.Register(kloepfelkoenig.de.Configuration.Routes);
        }
    }
}