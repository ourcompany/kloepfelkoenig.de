﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Optimization;
using System.Web.UI;
using ourCompany;

using Website.Tools;

namespace Website.Layouts
{
    public partial class Default : Base.PageBase
    {
        public CSSControl Content;
        public CSSControl Menu;

        private StringBuilder _CSS;
        private List<string> _FacebookImages;
        private StringBuilder _JS;
        public string Author { get; set; }
        public string CanonicalURL { get; set; }
        public StringBuilder CSS => _CSS ?? (_CSS = new StringBuilder());
        public string Description { get; set; }
        public List<string> FacebookImages => _FacebookImages ?? (_FacebookImages = new List<string>());
        public StringBuilder JS => _JS ?? (_JS = new StringBuilder());
        public string MainJSPath { get; set; }
        public string RequireJSPath { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            var metatags = new List<string>();

            if (Author.Check())
                metatags.Add("<meta name=\"author\" content=\"{0}\" />".With(Author));

            metatags.Add(Page.Header.Title.Check()
                ? "<meta property=\"og:title\" content=\"{0}\" />".With(Page.Header.Title)
                : "<meta property=\"og:title\" content=\"{0}\" />".With(Page.Title));

            if (Description.Check())
            {
                metatags.Add("<meta name=\"description\" content=\"{0}\" />".With(Description.Ellipsis(200)));
                metatags.Add("<meta property=\"og:description\" content=\"{0}\" />".With(Description.Ellipsis(200)));
            }

            if (CanonicalURL.Check())
            {
                metatags.Add("<link rel=\"canonical\" href=\"{0}\" />".With(CanonicalURL));
                metatags.Add("<meta property=\"og:url\" content=\"{0}\" />".With(CanonicalURL));
            }

            if (_BodyClasses != null && BodyClasses.Any())
                body.Attributes.Add("class", string.Join(" ", BodyClasses));

            if (_JSRequire != null && JSRequire.Any())
                body.Attributes.Add("data-require", string.Join(",", JSRequire));

            if (_FacebookImages != null && FacebookImages.Any())
                metatags.AddRange(FacebookImages.Select(facebookImage => "<meta property=\"og:image\" content=\"{0}\" />".With(facebookImage)));

            if (Context.IsDebuggingEnabled)
                MainJSPath = "/Scripts/main.js";
            else
                MainJSPath = "/Scripts/Build/" + Configuration.CurrentVersion + "/main.js";

            MetasPH.Controls.Add(new LiteralControl(string.Join(Environment.NewLine, metatags)));

            ScriptsPH.Controls.Add(new LiteralControl(
                (ScriptBundles.Any() ? Scripts.Render(ScriptBundles.Select(b => b.Path).ToArray()).ToHtmlString() : "") +
                (_JS != null ? "\n<script>{0}</script>".With(_JS.ToString()) : "")
                ));

            StylesPH.Controls.Add(new LiteralControl(
                (StyleBundles.Any() ? Styles.Render(StyleBundles.Select(b => b.Path).ToArray()).ToHtmlString() : "") +
                (_CSS != null ? "\n<style type=\"text/css\">{0}</style>".With(_CSS.ToString()) : "")
                ));

            RequireJSPath = Context.IsDebuggingEnabled ? "/Scripts/Lib/require.js" : "/Scripts/Build/" + Configuration.CurrentVersion + "/Lib/require.js";

            var stylesheetPath = Stylesheets.Select(p => "/Styles/" + (!Context.IsDebuggingEnabled ? "Build/" + Configuration.CurrentVersion + "/" : "") + p);

            if (stylesheetPath.Any())
                StylesPH.Controls.Add(new LiteralControl(Styles.Render(stylesheetPath.ToArray()).ToHtmlString()));
        }
    }
}