﻿using ourCompany.cms.Data;

namespace Website.App.Routing
{
    public interface IRouted
    {


        MongoDb Database { get; set; }

        MongoContext MongoContext { get; set; }


    }
}
