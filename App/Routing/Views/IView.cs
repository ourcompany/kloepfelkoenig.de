﻿using System.Threading.Tasks;
using System.Web;
using System.Web.Routing;
using Website.App.Routing.Routes;

namespace Website.App.Routing.Views
{

    public interface IView<T> : IRouted where T : class, IRouted, IHttpHandler, new()
    {

        T Layout { get; set; }

        RequestContext Context { get; set; }

        Route<T> Route { get; set; }

        Task<bool> Init();

        Task<bool> Render();
    }
}