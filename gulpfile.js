/// <binding Clean='cleanScripts, cleanStyles' ProjectOpened='default' />
var path = require('path');
var gulp = require('gulp');
var glob = require("glob");
var gutil = require('gulp-util');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var autoprefixer = require('gulp-autoprefixer');
var filesize = require('gulp-filesize');
var less = require('gulp-less');
var minifyCSS = require('gulp-minify-css');
var fs = require('fs');
var components = require('responsive-components');
var handlebars = require('gulp-handlebars');
var defineModule = require('gulp-define-module');
//var ts = require('gulp-typescript');
var changed = require('gulp-changed');
var watch = require('gulp-watch');
var notifier = require('node-notifier');
var amdOptimize = require('amd-optimize');
var through = require('through2');
var bundleConfigSrc = './Styles/bundles.config.json';
var componentsConfigSrc = './Styles/components.config.json';
var wrap = require('gulp-wrap');
var declare = require('gulp-declare');

// Standard handler
function standardErrorHandler(err) {
    // Notification
    notifier.notify({ message: 'Error: ' + err.message });
    // Log to console
    gutil.log(util.colors.red('Error'), err.message);
    this.emit('end');
}

gulp.task('clean:build', function () {
    return gulp
        .src(['Styles/Build', 'Scripts/Build'], { read: false })
        .pipe(clean());

});


gulp.task('themes', function () {
    // generate css files from less :
    glob("Styles/Themes/*.theme", {}, function (err, files) {
        files.forEach(function (file) {
            var content = fs.readFileSync(file, "utf-8");
            var themeConfig = JSON.parse(content.trim());
            gulp
                .src(['Styles/@theme.less'])
                .pipe(less({ modifyVars: themeConfig }))
                .pipe(rename(function (p) {
                    p.dirname += "/Themes";
                    p.basename = path.basename(file);
                    p.extname = ".css";
                }))
                .pipe(gulp.dest(function (f) {
                    return f.base;
                }))
                .on('end', lessBundles)
                .on('error', standardErrorHandler);
        });

    })

});

gulp.task('templates', function () {
    gulp.src('Templates/*.tpl')
      .pipe(handlebars())
      .pipe(defineModule('amd'))
      .pipe(gulp.dest('Templates/build/'));
});

gulp.task('less', function () {
    // generate css files from less :
    gulp
        .src(['Styles/**/*.less', '!**/@*'])
        .pipe(less())
        //.pipe(autoprefixer({
        //    browsers: ['last 2 versions'],
        //    cascade: false
        //}))
        .pipe(gulp.dest(function (f) {
            return f.base;
        }))
        .on('end', lessBundles)
        .on('error', standardErrorHandler);
});

function lessBundles() {
    var content = fs.readFileSync(bundleConfigSrc, "utf-8");

    var bundleConfig = JSON.parse(content.trim());
    // creates the bundles :

    for (var bundleName in bundleConfig) {
        gulp
            .src(bundleConfig[bundleName])
            .pipe(concat(bundleName + ".css"))
            .pipe(gulp.dest("Styles"));

        gulp
              .src(bundleConfig[bundleName])
              .pipe(minifyCSS())
              .pipe(concat(bundleName + ".css"))
              .pipe(gulp.dest("Styles/Build"));
    }
}

gulp.task('js', function () {
    gulp.src(['Scripts/**/*.js', '!Scripts/Build/**/*.js'])
        .pipe(uglify())
        .pipe(gulp.dest("Scripts/Build"))
        .on('error', standardErrorHandler);
});

gulp.task('components', function () {
    fs.readFile(componentsConfigSrc, "utf-8", function (err, data) {
        var componentsConfig = JSON.parse(data.trim());
        components.update(componentsConfig);
    });
});

gulp.task('release', ['clean:build'], function () {
    gulp.start(['js', 'less']);
});

gulp.task('default', function () {
    gulp.watch('Styles/**/*.less', ['less']);
    gulp.watch('Styles/@theme.less', ['themes']);
    gulp.watch('Styles/**/*.theme', ['themes']);
    gulp.watch('Templates/**/*.tpl', ['templates']);
});